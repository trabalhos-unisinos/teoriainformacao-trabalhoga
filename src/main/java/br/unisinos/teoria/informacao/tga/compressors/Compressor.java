package br.unisinos.teoria.informacao.tga.compressors;

import java.io.IOException;

public interface Compressor {

    byte[] compress(byte[] data) throws IOException;

    byte[] decompress(byte[] data) throws IOException;

}
