package br.unisinos.teoria.informacao.tga.compressors;

import java.io.IOException;

import com.sauljohnson.lizard.LzwCompressor;

public class LZW implements Compressor {

    private final LzwCompressor compressor = new LzwCompressor();

    public byte[] compress(final byte[] data) throws IOException {
        return compressor.compress(data);
    }

    public byte[] decompress(final byte[] data) throws IOException {
        return compressor.decompress(data);
    }
}
