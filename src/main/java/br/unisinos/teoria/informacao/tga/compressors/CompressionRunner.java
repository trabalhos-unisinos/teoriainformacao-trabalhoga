package br.unisinos.teoria.informacao.tga.compressors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.unisinos.teoria.informacao.tga.utils.FileUtils;
import br.unisinos.teoria.informacao.tga.utils.ListUtils;

/**
 * Classe executora dos métodos de compressão.
 *
 * Lê o arquivo, executa a compressão de acordo com o modo
 * e salva novamente o arquivo.
 */
public class CompressionRunner {

    private static final String COMPRESSOR_FILE_EXT = ".compressed";
    private static final CompressionRunner runner = new CompressionRunner();

    private final List<Compressor> compressors = new ArrayList<Compressor>();

    private CompressionRunner() {
        compressors.add(new LZW());
        compressors.add(new Huffman());
    }

    public void run(final String mode, final String filename) throws IOException {
        if (filename == null) {
            throw new RuntimeException("Invalid filename");
        }

        final byte[] filebytes = FileUtils.readFile(filename);

        byte[] result = null;
        String newFileName = null;

        if ("compress".equals(mode)) {
            result = compress(filebytes);
            newFileName = filename + COMPRESSOR_FILE_EXT;
        } else if ("decompress".equals(mode)) {
            result = decompress(filebytes);
            newFileName = filename.replace(COMPRESSOR_FILE_EXT, "");
        } else {
            throw new RuntimeException("Invalid compression mode");
        }

        FileUtils.writeFile(newFileName, result);
    }

    private byte[] compress(final byte[] filebytes) throws IOException {
        byte[] result = filebytes;

        for (final Compressor c : compressors) {
            result = c.compress(result);
        }

        return result;
    }

    private byte[] decompress(final byte[] filebytes) throws IOException {
        byte[] result = filebytes;

        // reverte lista para fazer descompressão ao contrário
        final List<Compressor> compressors = ListUtils.reverseList(this.compressors);

        for (final Compressor c : compressors) {
            result = c.decompress(result);
        }

        return result;
    }

    public static CompressionRunner getInstance() {
        return runner;
    }

}
