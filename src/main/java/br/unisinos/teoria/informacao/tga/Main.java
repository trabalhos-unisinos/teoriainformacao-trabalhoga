package br.unisinos.teoria.informacao.tga;

import static java.lang.String.format;

import java.io.IOException;

import br.unisinos.teoria.informacao.tga.compressors.CompressionRunner;

public class Main {

    public static void main(final String[] args) throws IOException {
        final String mode = args[0];
        final String filename = args[1];

        System.out.println(format("Iniciando %s", mode));

        CompressionRunner.getInstance().run(mode, filename);

        System.out.println(format("Arquivo %sed com sucesso", mode));
    }

}
