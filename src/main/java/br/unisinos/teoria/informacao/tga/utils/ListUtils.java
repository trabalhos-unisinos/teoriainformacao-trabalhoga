package br.unisinos.teoria.informacao.tga.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListUtils {

    public static <T> List<T> reverseList(final List<T> list) {
        final List<T> reverseList = new ArrayList<T>(list);
        Collections.reverse(reverseList);
        return reverseList;
    }

}
