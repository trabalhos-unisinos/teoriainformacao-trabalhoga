package br.unisinos.teoria.informacao.tga.utils;

import static org.apache.commons.io.FileUtils.readFileToByteArray;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;

import java.io.File;
import java.io.IOException;

public class FileUtils {

    public static byte[] readFile(final String filename) throws IOException {
        final File file = new File(filename);
        return readFileToByteArray(file);
    }

    public static void writeFile(final String filename, final byte[] data) throws IOException {
        final File file = new File(filename);
        writeByteArrayToFile(file, data);
    }

}
