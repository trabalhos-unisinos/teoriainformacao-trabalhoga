package br.unisinos.teoria.informacao.tga.compressors;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import nayuki.huffmancoding.BitInputStream;
import nayuki.huffmancoding.BitOutputStream;
import nayuki.huffmancoding.CanonicalCode;
import nayuki.huffmancoding.CodeTree;
import nayuki.huffmancoding.FrequencyTable;
import nayuki.huffmancoding.HuffmanCompress;
import nayuki.huffmancoding.HuffmanDecompress;

public class Huffman implements Compressor {

    private static int SYMBOL_LIMIT = 257;

    /**
     * Huffaman compress - based on https://github.com/nayuki/Reference-Huffman-coding/blob/master/java/src/HuffmanCompress.java
     */
    public byte[] compress(byte[] data) throws IOException {
        // Read byte array once to compute symbol frequencies.
        // The resulting generated code is optimal for static Huffman coding and also canonical.
        final FrequencyTable freqs = getFrequencies(data);
        freqs.increment(SYMBOL_LIMIT - 1); // EOF symbol gets a frequency of 1

        CodeTree code = freqs.buildCodeTree();
        final CanonicalCode canonCode = new CanonicalCode(code, SYMBOL_LIMIT);
        // Replace code tree with canonical one. For each symbol,
        // the code value may change but the code length stays the same.
        code = canonCode.toCodeTree();

        // Read byte array again, compress with Huffman coding, and write output file
        final InputStream in = new ByteArrayInputStream(data);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final BitOutputStream bitOut = new BitOutputStream(out);

        writeCodeLengthTable(bitOut, canonCode);
        HuffmanCompress.compress(code, in, bitOut);

        bitOut.close();
        return out.toByteArray();
    }

    private static FrequencyTable getFrequencies(final byte[] data) throws IOException {
        // Returns a frequency table based on the bytes in the given file.
        // Also contains an extra entry for symbol 256, whose frequency is set to 0.
        final FrequencyTable freqs = new FrequencyTable(new int[SYMBOL_LIMIT]);
        final InputStream input = new BufferedInputStream(new ByteArrayInputStream(data));

        while (true) {
            int b = input.read();
            if (b == -1) {
                break;
            }
            freqs.increment(b);
        }

        return freqs;
    }

    private void writeCodeLengthTable(final BitOutputStream out, final CanonicalCode canonCode) throws IOException {
        for (int i = 0; i < canonCode.getSymbolLimit(); i++) {
            int val = canonCode.getCodeLength(i);
            // For this file format, we only support codes up to 255 bits long
            if (val >= SYMBOL_LIMIT - 1) {
                throw new RuntimeException("The code for a symbol is too long");
            }

            // Write value as 8 bits in big endian
            for (int j = 7; j >= 0; j--) {
                out.write((val >>> j) & 1);
            }
        }
    }

    /**
     * Huffman decompress - based on https://github.com/nayuki/Reference-Huffman-coding/blob/master/java/src/HuffmanDecompress.java
     */
    public byte[] decompress(final byte[] data) throws IOException {
        // Perform file decompression
        final BitInputStream in = new BitInputStream(new ByteArrayInputStream(data));
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final CanonicalCode canonCode = readCodeLengthTable(in);
        final CodeTree code = canonCode.toCodeTree();

        HuffmanDecompress.decompress(code, in, out);

        return out.toByteArray();
    }

    private CanonicalCode readCodeLengthTable(final BitInputStream in) throws IOException {
        final int[] codeLengths = new int[SYMBOL_LIMIT];

        for (int i = 0; i < codeLengths.length; ++i) {
            int val = 0;
            for (int j = 0; j < 8; ++j) {
                val = val << 1 | in.readNoEof();
            }
            codeLengths[i] = val;
        }

        return new CanonicalCode(codeLengths);
    }
}
