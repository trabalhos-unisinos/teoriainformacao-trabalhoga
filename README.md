# Trabalho do Grau A - Teoria da Informação

Projeto implementa compressão e descompressão de arquivos com Java utilizando os algoritmos LZW e Huffman.

## Estrutura do Projeto

O projeto inclui uma interface chamada `Compressor` com o método de compressão e descompressão de byte arrays na qual os algoritmos de compressão devem implementar para que possa ser utilizado no `CompressionRunner`, que possui uma lista de compressores a serem utilizados.

No projeto, foram implementados os algoritmos LZW e Huffman, utilizando bibliotecas open-source descritas nas referências. 

No modo de compressão, primeiramente é executado o LZW sobre o arquivo e após, o algoritmo de Huffman. Na descompressão, entretando, a ordem dos algorimos é reversa, sendo primeiro executado o Huffman e após o LZW.

## Executando o projeto

### Pré-requisitos

- Maven
- Java

### Gerando o JAR

1. Clone o projeto
2. Acesse a pasta do projeto
3. Execute o comando `mvn clean install`

Após esses comandos, o arquivo JAR será gerado na pasta `target/`

### Executando o projeto

Para executar o projeto, basta utilizar o comando `java -jar target/tga-1.0.jar {0} {1}` onde:

- `{0}` significa o modo de compressão: `compress` ou `decompress`
- `{1}` significa o path do arquivo

Caso o modo de compressão seja `compress`, o arquivo será comprimido e o resultado, salvo no mesmo path do arquivo original com o mesmo nome + o sufixo `.compressed`.

Caso o modo de compressão seja `decompress`, o arquivo será descomprimido e o resulto, salvo no mesmo path do arquivo original sem o sufixo `.compressed` no nome.

## Resultados finais

Utilizando os arquivos de teste, foram obtidos os seguintes resultados:

|Arquivo|Tamanho original|Tamanho comprimido|Diferença de tamanho|
|:-:|:-:|:-:|:-:|
|alice29.txt|149KB|69KB|53.69% de redução|
|sum|38KB|22KB|42.10% de redução|

## Referências

### ByteCompressor: Huffman Coding - https://github.com/avast/bytecompressor

Biblioteca disponibilizada pelo Avast utilizada para a implementação do algoritmo de Huffman.

### Lizard - https://github.com/lambdacasserole/lizard

Biblioteca disponibilizada por Saul Johnson utilizada para a compressão com o algoritmo Lempel–Ziv–Welch.

### Commons IO - https://commons.apache.org/proper/commons-io/

Biblioteca de utilitários disponibilizada pelo Apacha utilizada para a leitura e escrita de arquivos em byte array.
